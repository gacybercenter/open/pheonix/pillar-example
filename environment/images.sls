#!yaml|gpg

# NOTE(chateaulav): This provides a list of images that should be present
#                   on controllers/caches
#
#  example:
#    name: extracted value on disk
#    type: type of image (url, virt-builder, etc)
#    url: URL if cache not present (required for type=url)

images:
  ubuntu2004:
    name: ubuntu2004
    type: url
    url: https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img