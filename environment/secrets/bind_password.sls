#!yaml|gpg

# NOTE(chateaulav): Create similar files for other passwords you want to
#                   encrypt. The file name should be the name of the name of
#                   the service you want to encrypt the password for.
#                   'service_password.yml' for example.

bind_password: |
  -----BEGIN PGP MESSAGE-----

  <ENCRYPTED KEY HERE>
  -----END PGP MESSAGE-----