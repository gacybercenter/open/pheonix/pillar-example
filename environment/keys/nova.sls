#!yaml|gpg

# NOTE(chateaulav): This provides a base key to enable use of the
#                   Masakari service allowing for live migration of
#                   instances in the event of a host failure.

nova_key: |
  -----BEGIN PGP MESSAGE-----

  <ENCRYPTED KEY HERE>
  -----END PGP MESSAGE-----