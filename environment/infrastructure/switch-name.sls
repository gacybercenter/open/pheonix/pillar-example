#!yaml|gpg

# This is a sample configuration file for the Juniper JProxy service
# It is used to configure the service and the proxy itself

proxy:
  password: |
    -----BEGIN PGP MESSAGE-----
    <ENCRYPTED KEY HERE>
    -----END PGP MESSAGE-----
  host: <IP-ADDRESS-HERE>
  username: <USERNAME>
  proxytype: junos