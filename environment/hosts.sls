# NOTE(chateaulav): This provides a template for the configuration file. It is
# not intended to be used as-is. Outline below are the various key:value pairs
# that are required for the physical and virtual host definitions.
#
# Required Parameters:
#
#   style: physical and virtual - the type of host you are defining
#   enabled: True and False - whether or not to deploy this host
#   os: ubuntu2004, centos8, and centos7 - the OS you want to deploy
#   disk: str - the size of the Virtual Disk (Gigabytes) or the Physical Disk to install OS on (e.g. Micron_9300_MTFDHAL7T6TDP)
#   interface: str - the interface you want to use when PXE booting (should be DHCP enabled)
#   networks: {dict} - the networks you want to configure on the host
#     management: {dict} - the management network
#       managed: bool - whether or not the builtin networking state should manage the configuration (defaults to True)
#       bridge: bool - whether or not the interface should be bridged (implicit false)
#       interfaces: [list] - the interfaces you want to bind to the management network, If the list is <1, it implies a bond.
#     sfe: {dict} - the Storage Frontend network
#       interfaces: [list] - the interfaces you want to bind to the sfe network
#     public: {dict} - the Public network
#       interfaces: [list] - the interfaces you want to bind to the public network
#     private: {dict} - the Private network
#       interfaces: [list] - the interfaces you want to bind to the private network
#     sbe: {dict} - the Storage Backend network
#       interfaces: [list] - the interfaces you want to bind to the sbe network
#
#
#   Phycial hosts:
#     role: controller, compute, storage, and container
#     root_password_crypted: str - the preseeded password for the root user. See mkpasswd.
#     ntp_server: str - the ntp server you want to use for the preseed process
#     proxy: str - the proxy you want to use for the preseed process (static, external proxy, or pull_from_mine)
#     uuids:
#       - list of all the uuids belonging to this type of host
#
#   Controller Only:
#     kvm_disk_config: {dict} - the configuration for the kvm disk
#       type: raid1, raid10, or standard - the type of disk configuration you want to use
#       members: [list] - the members of the disk configuration
#        <Available Options by Type>
#        - str - the device you want to use for the disk configuration (standard only)
#        - str - the special value 'rootfs' to use the root filesystem for the disk configuration,
#                  which will create a directory at '/kvm' on the root of the filesystem (standard only)
#        - [list] - the list of devices you want to use for the disk configuration (raid1 and raid10 only)
#
#
#   Virtual hosts:
#     count: int - the number of virtual hosts you want to deploy
#     ram: int - the amount of ram you want to allocate to the virtual host (Kilobytes)
#     cpu: int - the number of cpus you want to allocate to the virtual host
#     disk: str - the size of the disk you want to allocate to the virtual host (Gigabytes)

hosts:
  controller:
    style: physical
    role: controller
    enabled: False
    os: ubuntu2004
    uuids:
      - PLACE-PHYSICAL-SERVER-UUID-NUMBER-HERE
    interface: enp####
    proxy: pull_from_mine
    root_password_crypted: PLACE-ENCRYPTED-PASSWORD-HERE
    ntp_server: 0.us.pool.ntp.org
    disk: PLACE-HDD-TYPE-HERE
    kvm_disk_config:
      type: standard
      members:
        - rootfs
    networks:
      management:
        interfaces: [enp####]
        bridge: true
      sfe:
        interfaces: [enp####]
        bridge: true
      public:
        interfaces: [enp####]
        bridge: true
      private:
        interfaces: [enp####]
        bridge: true
  storage:
    style: physical
    role: storage
    enabled: True
    os: ubuntu2004
    uuids:
      - PLACE-PHYSICAL-SERVER-UUID-NUMBER-HERE
    interface: ens####
    proxy: pull_from_mine
    root_password_crypted: PLACE-ENCRYPTED-PASSWORD-HERE
    ntp_server: 0.us.pool.ntp.org
    disk: PLACE-HDD-TYPE-HERE
    networks:
      management:
        interfaces: [ens####]
      sfe:
        interfaces: [ens####]
      sbe:
        interfaces: [ens####, ens####]
  compute:
    style: physical
    role: compute
    enabled: True
    os: ubuntu2004
    uuids:
      - PLACE-PHYSICAL-SERVER-UUID-NUMBER-HERE
    interface: enp####
    proxy: pull_from_mine
    root_password_crypted: PLACE-ENCRYPTED-PASSWORD-HERE
    ntp_server: 0.us.pool.ntp.org
    disk: PLACE-HDD-TYPE-HERE
    networks:
      management:
        interfaces: [enp####]
      sfe:
        interfaces: [enp####]
      public:
        interfaces: [enp####]
      private:
        interfaces: [enp####]
  container:
    style: physical
    role: container
    enabled: True
    os: ubuntu2004
    uuids:
      - PLACE-PHYSICAL-SERVER-UUID-NUMBER-HERE
    interface: enp####
    proxy: pull_from_mine
    root_password_crypted: PLACE-ENCRYPTED-PASSWORD-HERE
    ntp_server: 0.us.pool.ntp.org
    disk: PLACE-HDD-TYPE-HERE
    networks:
      management:
        interfaces: [enp####]
      sfe:
        interfaces: [enp####]
      public:
        interfaces: [enp####]
      private:
        interfaces: [enp####]
  cache:
    style: virtual
    enabled: False
    count: 1
    ram: 8192000
    cpu: 2
    os: ubuntu2004
    disk: 512G
    networks:
      management:
        interfaces: [ens3]
  cephmon:
    style: virtual
    enabled: True
    count: 3
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  mds:
    style: virtual
    enabled: False
    count: 3
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  haproxy:
    style: virtual
    enabled: False
    count: 1
    ram: 16384000
    cpu: 16
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  mysql:
    style: virtual
    enabled: True
    count: 3
    ram: 65536000
    cpu: 32
    os: ubuntu2004
    disk: 128G
    networks:
      management:
        interfaces: [ens3]
  rabbitmq:
    style: virtual
    enabled: True
    count: 3
    ram: 32768000
    cpu: 32
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  memcached:
    style: virtual
    enabled: True
    count: 3
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  keystone:
    style: virtual
    enabled: True
    count: 3
    ram: 16384000
    cpu: 8
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  glance:
    style: virtual
    enabled: True
    count: 2
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  nova:
    style: virtual
    enabled: True
    count: 3
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 128G
    networks:
      management:
        interfaces: [ens3]
  neutron:
    style: virtual
    enabled: True
    count: 3
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  horizon:
    style: virtual
    enabled: True
    count: 2
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 128G
    networks:
      management:
        interfaces: [ens3]
  heat:
    style: virtual
    enabled: True
    count: 2
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  cinder:
    style: virtual
    enabled: True
    count: 2
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  volume:
    style: virtual
    enabled: True
    count: 3
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  designate:
    style: virtual
    enabled: True
    count: 3
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  bind:
    style: virtual
    enabled: True
    count: 3
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  swift:
    style: virtual
    enabled: True
    count: 2
    ram: 16384000
    cpu: 8
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  zun:
    style: virtual
    enabled: True
    count: 2
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  placement:
    style: virtual
    enabled: True
    count: 2
    ram: 16384000
    cpu: 8
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  opensearch:
    style: virtual
    enabled: False
    count: 1
    ram: 32768000
    cpu: 4
    os: ubuntu2004
    disk: 512G
    networks:
      management:
        interfaces: [ens3]
  network:
    style: virtual
    enabled: True
    count: 3
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      private:
        interfaces: [ens4]
      public:
        interfaces: [ens5]
  ovsdb:
    style: virtual
    enabled: False
    count: 3
    ram: 32768000
    cpu: 16
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  barbican:
    style: virtual
    enabled: False
    count: 2
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  magnum:
    style: virtual
    enabled: False
    count: 2
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  sahara:
    style: virtual
    enabled: False
    count: 2
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  manila:
    style: virtual
    enabled: False
    count: 3
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  share:
    style: virtual
    enabled: False
    count: 3
    ram: 4096000
    cpu: 2
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
      sfe:
        interfaces: [ens4]
  etcd:
    style: virtual
    enabled: True
    count: 3
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  guacamole:
    style: virtual
    enabled: True
    count: 3
    ram: 16384000
    cpu: 8
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]
  jproxy:
    style: virtual
    enabled: True
    count: 1
    ram: 8192000
    cpu: 4
    os: ubuntu2004
    disk: 64G
    networks:
      management:
        interfaces: [ens3]