# These files should only be touched by advanced users.
# They control how/when various hosts notify other hosts about their
# internal goings-on (update addresses in haproxy, edit permissions
# in mysql, etc.)

mine_functions:
  network.ip_addrs: []
  build_phase:
    - mine_function: grains.get
    - build_phase