#!yaml|gpg

# Example Pillar Configuration file for the Pheonix Orchestration.

# URL to the primary Phenoix repo (usually https://gitlab.com/gacybercenter/open/pheonix/states.git).
# The branch key can be a branch, tag, or commit sha
gitfs_remote_configuration:
  url: https://gitlab.com/gacybercenter/open/pheonix/states.git
  branch: master

# URL to your external pillar (can be on any publicly-accessible version control system)
# The branch key can be a branch, tag, or commit sha
gitfs_pillar_configuration:
  url: https://<URL-TO-YOUR-PILLAR>.git
  branch: dev

# Other remotes that you need on top of the default (security configuration, etc.)
# Example:
# gitfs_other_configurations:
#   security:
#     url: https://somerepohost.whatever/security.git
#     branch: master
#   other_thing:
#     url: https://somerepohost.whatever/other_thing.git
#     branch: master
gitfs_other_configurations: {}

# Salt configuration
# DNS A record that points to your salt master for this environment
# Valid salt versions compatible are 3000, 3001, 3002, latest, $MAJOR, or $MAJOR.$PATCH)
salt:
  version: 3004
  record: salt.internal.range.com
  name: salt
  conf:
    cpu: 6
    ram: 8192000
    disk: 16G
    interface: mgmt

# PXE configuration
# DNS A record that points to your pxe host for this environment
pxe:
  record: pxe.internal.range.com
  name: pxe
  conf:
    cpu: 1
    ram: 2048000
    disk: 16G
    interface: mgmt

# Specify your timezone
# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.timezone.html
timezone: America/New_York

# Specify NTP servers
ntp:
  ntp_server: time-d-g.nist.gov
  ntp_fallback: time-e-g.nist.gov

# Set your orchestration timeouts
# generate is how long a generate runner will wait until it gets a phase check
# event, e.g. {{ type }}/generate/auth/start
# phasecheck configures the dependence retry states. You can increase interval and splay
# and decrease attempts to lower the load on your salt-master somewhat at the cost
# of overall orchestration completion speed
timeouts:
  generate: 7200
  phasecheck:
    interval: 30
    attempts: 240
    splay: 10

# Specify your endpoint URLs for openstack
endpoints:
  public: dashboard.range.com
  internal: dashboard.range.com
  admin: dashboard.range.com

# Specify your ldap configuration
common_ldap_configuration:
  address: directory.fix.me.please
  bind_user: uid=bind,cn=users,cn=accounts,dc=foo,dc=bar
  base_dn: dc=foo,dc=bar
  user_dn: cn=users,cn=accounts,dc=foo,dc=bar
  group_dn: cn=groups,cn=accounts,dc=foo,dc=bar

# keystone-specific config.  user_filter should be a group that all range users
# are a member of.  group_filter should be a group that all range groups are a member
# of.  Keystone_domain is the domain you want to use to access your LDAP accounts on
# the horizon login page
keystone:
  ldap_enabled: False
  ldap_configuration:
    user_filter: (memberOf=cn=foo_user_filter,cn=groups,cn=accounts,dc=bar,dc=baz)
    group_filter: (memberOf=cn=foo_group_filter,cn=groups,cn=accounts,dc=bar,dc=baz)
    keystone_domain: ipa
  token_expiration: 14400

# Specify your haproxy TLS options
# The group variable is for setting the danos resource group name for haproxy so that you can run 
# multiple environments off the same infrastructure
haproxy:
  group: range
  acme_email: <COMPANY-EMAIL>
  dashboard_domain: dashboard.range.com
  console_domain: console.range.com
  guacamole_domain: range.com

# Specify which keys you would like to be added to authorized_keys for the root user on ALL machines
# https://docs.saltstack.com/en/latest/ref/states/all/salt.states.ssh_auth.html
authorized_keys:
  ENCODED-PUBLIC-SSH-KEY-ADMIN-ACCESS
    encoding: ssh-rsa

nova_live_migration_auth_key:
  ENCODED-PUBLIC-SSH-KEY-LIVE-MIGRATION
    encoding: ssh-rsa

# Integrated OpenSearch Configuration
# enabled: True or False
# record: FQDN that maps to Central Logging Server
# name: ALIAS that maps to Central Logging Server
# See environment/answers.sls to enable Built-in server
# and ensure DNS maps to the defined record
fluentd:
  enabled: True
  record: logger.<DOMAIN-NAME>
  name: logger


# Specify the IPMI user that will be used to bootstrap physical devices
api_user: ADMIN

# Integrated TNSR Configuration
# Enabled: True or False
# username: the TNSR user that will access the REST API
# endpoint: Can either be the special value of 'gateway' (which will resolve to the ip address of the default gateway on the management network) or an IP
# See environment/tnsr_password.sls for password configuration
tnsr:
  enabled: False
  username: root
  endpoint: <ENDPOINT-IP-ADDRESS>

# Specify your subnets.  The number of addresses for private, sfe, sbe, and oob should be
# equivalent to the number of addresses in management (and management should be at least a /24)
# The public subnet should be the already-existing network that you will utilize to grant
# external access to your instances.

# float_start - Where you want your DHCP leases for your public network to begin
# float_end - Where you want your DHCP leases for your public network to end
# float_gateway - Gateway for your public network
# float_dns - DNS for your float network
# cache/dns_public_IP - currently unparsed

networking:
  subnets:
    management: 10.200.1.0/24
    public: 10.201.0.0/16
    private: 10.200.4.0/24
    sfe: 10.200.2.0/24
    sbe: 10.200.3.0/24
    oob: 10.100.0.0/24
  addresses:
    float_start: 10.201.20.0
    float_end: 10.201.255.100
    float_gateway: 10.201.255.254
    float_dns: 10.100.10.5

# neutron networking backend.  Valid values are networking-ovn
# or linuxbridge or openvswitch
# added configurable items that should be the operators choice instead of 
# harcoded values
neutron:
  backend: openvswitch
  max_l3_agents_per_router: 2
  dhcp_agents_per_network: 2
  l3ha: True
  openvswitch:
    type_drivers: flat,vlan,vxlan
    tenant_network_types: vxlan
    mechanism_drivers: openvswitch,l2population
    extension_drivers: port_security,qos,dns_domain_ports
    vni_ranges: 1:65536
    extensions: qos

# nova configuration options.  Ref: https://docs.openstack.org/nova/latest/configuration/config.html
nova:
  token_ttl: 3600

# the theme you wish to install in horizon (set url to false if none).  URL should point to git repo
# name should be the top-level directory you wish to extract the theme to
# Site branding and site link should match appropriate values.
# See https://docs.openstack.org/horizon/latest/configuration/customizing.html
horizon:
  theme:
    url: https://<URL-TO-HORIZOM-THEME-REPO>
    branch: master
    name: <TLD to extract Theme to>
    site_branding: <COMPANY NAME>
    site_branding_link: https://<COMPANY-WEBSITE>
  session_timeout: 7200

# specify which docker image you would wish to use for the cloud shell functionality
zun:
  cloud_shell_image: usacys/openstack-client:latest

# Number of placement groups for your ceph pools
# https://docs.ceph.com/docs/master/rados/operations/placement-groups/
cephconf:
  autoscale: 'off'
  vms_pgs: 2048
  volumes_pgs: 512
  images_pgs: 512
  fileshare_data_pgs: 512
  fileshare_metadata_pgs: 128

# Set Top Level Domain restrictions for Desigante
designate:
  tld: gcr-dev

# Assorted salt master configuration options.  Each entry will be written to a separate file in /etc/salt/master.d
# https://docs.saltstack.com/en/latest/ref/configuration/master.html
master-config:
  default_top: |
    default_top: base
  file_roots: |
    file_roots:
      base:
        - /srv/salt/
  fileserver_backend: |
    fileserver_backend:
      - git
      - roots
  gitfs_update_interval: |
    gitfs_update_interval: 1800
  hash_type: |
    hash_type: sha512
  interface: |
    interface: 0.0.0.0
  loop_interval: |
    loop_interval: 10
  ping_on_rotate: |
    ping_on_rotate: True
  state_output: |
    state_output: changes
  top_file_merging_strategy: |
    top_file_merging_strategy: same
  pillar_roots: |
    pillar_roots:
      base:
        - /srv/dynamic_pillar
  sock_pool_size: |
    sock_pool_size: 4
  external_auth: |
    external_auth:
      pam:
        api:
          - 'salt-dev':
            - address.*
  rest_cherrypy: |
    rest_cherrypy:
      port: 8000
      ssl_crt: /etc/pki/tls/certs/localhost.crt
      ssl_key: /etc/pki/tls/certs/localhost.key
  runner_dirs: |
    runner_dirs:
      - /srv/runners
  reactor: |
    reactor:
      - salt/minion/*/start:
        - salt://reactor/update_mine.sls
        - salt://reactor/highstate_notification.sls
        - salt://reactor/update_ceph_conf.sls
      - salt/beacon/*/network_settings/result:
        - salt://reactor/update_mine.sls
        - salt://reactor/highstate_notification.sls
        - salt://reactor/update_ceph_conf.sls
      - salt/beacon/*/inotify//var/log/apt-cacher-ng/apt-cacher.err:
        - salt://reactor/acng_maintenance.sls
      - set/images/pool_pgs:
        - salt://reactor/set_images_pool_pgs.sls
      - set/vms/pool_pgs:
        - salt://reactor/set_vms_pool_pgs.sls
      - set/volume/pool_pgs:
        - salt://reactor/set_volumes_pool_pgs.sls
      - set/manila/pool_pgs:
        - salt://reactor/set_manila_pool_pgs.sls
